require 'course'

class Student

    attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    first_name + " " + last_name
  end

  def courses
    @courses
  end

  def enroll(new_course)
    if check_conflict(new_course)
      courses << new_course unless courses.include?(new_course)
      new_course.students << self
    else
      raise_error
    end
    #new_course.add_student(self)

  end

  def course_load
    course_load = {}
    courses.each do |course|
      if course_load[course.department] == nil
        course_load[course.department] = course.credits
      else
        course_load[course.department] += course.credits
      end
    end
    course_load
  end

  def check_conflict(new_course)
    courses.each do |course|
      return false if course.conflicts_with?(new_course)
    end
    return true
  end

  def raise_error
    raise "course would cause conflict!"
  end
end
